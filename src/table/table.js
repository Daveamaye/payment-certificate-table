import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { DataGrid, GridColDef } from '@mui/x-data-grid';

const Table = () => {
  const [data, setData] = useState([]);
  const [showAdditionalInfo, setShowAdditionalInfo] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('https://stagingpayment.chipchip.social/v1/system/payments?per_page=5&page=0&filter=[{"column_field":"amount", "operator_value":"=", "value":"18.43"}]', {
          headers: {
            'Authorization': 'Bearer eyJhbGciOiJQUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMGY3OWEzZC04NmQ4LTQ3NjEtYjc5ZS1hMzEyOWU0OGIwYTUiLCJuYmYiOjE3MTY5ODAyNzR9.J7W4g36cWFi7Xg2J0Ehw3XPgQTJfRmwK840gboOH6PUOdDxPSeUFj4Nyq6Uu1rdoM3Ohf3t1g0PZTSRyq5tWweFxcnfkwgFkvPWgp7ILnZoCWUoXAIT81M3zkQiN-79EfQxjT2h0EKPobGxf1ZKwERdZ7iST5YBfoshoaNIIj6m955rYMRjz1fI5-U2ysgaxvmAmRNdEz2DFbgWsawid-kAF31bJz_KFCCtiFm1t8MQW39ivi1UzezHvZ1zqn0sTbiicb4f0Q16J0kJ5rP2XUO0rM9S3hHGuktcUFhSd65MOWODhFzy5qSZwhLqd7TVha5cExz3BpDmUooOu5it4mA'
          }
        });
        
        setData(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  },[]);

  const toggleAdditionalInfo = (index) => {
    setShowAdditionalInfo((prevState) => {
      const newState = [...prevState];
      newState[index] = !newState[index];
      return newState;
    });
  };

  // return (
  //   <div>
  //     <h1>Customer Data Table</h1>
  //     <table>
  //       <thead>
  //         <tr>
  //           <th>Service Name</th>
  //           <th>Amount</th>
  //           <th>Customer Phone</th>
  //           <th>Status</th>
  //           <th>Additional Info</th>
  //         </tr>
  //       </thead>
  //       <tbody>
  //         {data && data.length > 0 ? (
  //           data.map((item, index) => (
  //             <React.Fragment key={index}>
  //               <tr>
  //                 <td>{item.name}</td>
  //                 <td>{item.amount}</td>
  //                 <td>{item.customer_phone}</td>
  //                 <td>{item.status}</td>
  //                 <td>
  //                   <button onClick={() => toggleAdditionalInfo(index)}>
  //                     {showAdditionalInfo[index] ? 'Hide Info' : 'Show Info'}
  //                   </button>
  //                 </td>
  //               </tr>
  //               {showAdditionalInfo[index] && (
  //                 <tr>
  //                   <td colSpan="5">
  //                     <div>
  //                       <p>Id: {item.id}</p>
  //                       <p>Third party payment id: {item.third_party_payment_id}</p>
  //                       <p>Service Id: {item.service_id}</p>
  //                       <p>Description: {item.description}</p>
  //                       <p>Bill reference number: {item.bill_reference_number}</p>
  //                       <p>Created at: {item.created_at}</p>
  //                       <p>Updated at: {item.updated_at}</p>
  //                       <p>Expire at: {item.expire_at}</p>
  //                     </div>
  //                   </td>
  //                 </tr>
  //               )}
  //             </React.Fragment>
  //           ))
  //         ) : (
  //           <tr>
  //             <td colSpan="5">No data available.</td>
  //           </tr>
  //         )}
  //       </tbody>
  //     </table>
  //   </div>
  // );

  const columns  = [
    { field: 'id', headerName: 'ID' },
    { field: 'payment_type', headerName: 'Payment Type' },
    { field: 'service_name', headerName: 'Service Name' },
    { field: 'amount', headerName: 'Amount' },
  ];

  return (
    <div style={{ height: 400, width: '100%' }}>
    <DataGrid
      rows={data}
      columns={columns}
      initialState={{
        pagination: {
          paginationModel: { page: 0, pageSize: 5 },
        },
      }}
      pageSizeOptions={[5, 10]}
      checkboxSelection
    />
  </div>
);
}
export default Table;