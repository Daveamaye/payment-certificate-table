import logo from './logo.svg';
import './App.css';
import Table from './table/table.js'
import { TableHeader } from './table/table';

const App = () => {
  return (
    <div className="App">
      <Table />
    </div>
  );
}

export default App;
